locals {
  repo_config = jsondecode(file("${path.module}/data/gitlab-github.json"))
  repos       = keys(local.repo_config)
}

resource "github_repository_deploy_key" "deploy_keys" {
  for_each   = toset([for repo in github_repository.repo : repo.name if lookup(local.repo_config[repo.name], "key", false) != false])
  repository = each.key
  key        = local.repo_config[each.key]["key"]
  title      = "gitlab"
  read_only  = false
}

resource "github_repository" "repo" {
  for_each               = toset(local.repos)
  name                   = each.key
  allow_auto_merge       = false
  allow_merge_commit     = true
  allow_rebase_merge     = true
  allow_squash_merge     = true
  archive_on_destroy     = true
  archived               = false
  delete_branch_on_merge = false
  has_downloads          = false
  has_issues             = false
  has_projects           = false
  has_wiki               = false
  vulnerability_alerts   = true

  topics = distinct(concat(lookup(local.repo_config[each.key], "topics", []), ["mirrored-repository"]))

  description = format("Github mirror of %s - our actual code is hosted with %s (please see https://www.mediawiki.org/wiki/Developer_access for contributing)",
    each.key,
    lookup(local.repo_config[each.key], "key", false) == false ? "Gerrit" : "Gitlab"
  )
}
