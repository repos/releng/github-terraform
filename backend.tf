/**
 * Copyright (c) 2022 Wikimedia Foundation
 *
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */

terraform {
  required_providers {
    github = {
      source  = "integrations/github"
      version = "~> 4.0"
    }
  }

  backend "http" {}
}
