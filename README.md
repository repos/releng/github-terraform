# Github Terraform

This repository handles our Github organization via Terraform. Gitlab CI/CD handles this automatically for us. Planning is done on every PR. Applying requires a manual trigger because it has the potential for being destructive.
