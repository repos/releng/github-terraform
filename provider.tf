/**
 * Copyright © 2022 Wikimedia Foundation
 *
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */

provider "github" {
  owner = "wikimedia"
}
